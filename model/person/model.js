const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PersonSchema = new Schema({
    name: {type: String, required: true},
    church: String,
    observation: String,
    partners: [{type: Schema.Types.ObjectId, ref: 'partner'}]
});

module.exports = mongoose.model('person', PersonSchema);