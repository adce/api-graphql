const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PartnerSchema = new Schema({
    name: {type: String, required: true},
    relationship: String,
    person: {type: Schema.Types.ObjectId, ref: 'person'}
});

module.exports = mongoose.model('partner', PartnerSchema);