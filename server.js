const { makeExecutableSchema } = require('graphql-tools');
const { ApolloServer, PubSub } = require('apollo-server');
const glue = require('schemaglue');
global.mongoose = require('mongoose');

const pubsub = new PubSub();
const { schema, resolver } = glue('./model');
const executable = makeExecutableSchema({
    typeDefs: schema,
    resolvers: resolver
});

mongoose.connect('mongodb://localhost/visitors');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'error connection'));
db.once('open', () => { console.log('connection monbodb success...')});

const server = new ApolloServer({
    schema: executable,
    context: ({ req, res }) => ({ req, res, pubsub })
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`)
  });
